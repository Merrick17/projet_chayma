import { Component, OnInit, Input, ViewChild } from '@angular/core';
import * as Chartist from 'chartist';
import { NgbModal, NgbDatepicker } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  stringValue; 
  evnt:any=[]; 
events:any=[]=[{
'Titre':'Event 1', 
'date_debut':'demain',
'date_fin':'ghodwaaa'
}, {
  'Titre':'Event 1', 
  'date_debut':'demain',
  'date_fin':'ghodwaaa'
  }, {
    'Titre':'Event 1', 
    'date_debut':'demain',
    'date_fin':'ghodwaaa'
    }]; 
    model:any ;
    dateOfBirth:string ;  
    title:string ; 
    desc:string ; 
    date_debut:any ; 
    date_fin:any;
    role="admin"; 
    clients:any=[]; 
    designation:string ; 
    raison:string ; 
    creditID:string ; 
    papers:any=[];
  constructor( private modalService: NgbModal , private htpp:HttpClient) { }
  startAnimationForLineChart(chart){
      let seq: any, delays: any, durations: any;
      seq = 0;
      delays = 80;
      durations = 500;

      chart.on('draw', function(data) {
        if(data.type === 'line' || data.type === 'area') {
          data.element.animate({
            d: {
              begin: 600,
              dur: 700,
              from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
              to: data.path.clone().stringify(),
              easing: Chartist.Svg.Easing.easeOutQuint
            }
          });
        } else if(data.type === 'point') {
              seq++;
              data.element.animate({
                opacity: {
                  begin: seq * delays,
                  dur: durations,
                  from: 0,
                  to: 1,
                  easing: 'ease'
                }
              });
          }
      });

      seq = 0;
  };
  startAnimationForBarChart(chart){
      let seq2: any, delays2: any, durations2: any;

      seq2 = 0;
      delays2 = 80;
      durations2 = 500;
      chart.on('draw', function(data) {
        if(data.type === 'bar'){
            seq2++;
            data.element.animate({
              opacity: {
                begin: seq2 * delays2,
                dur: durations2,
                from: 0,
                to: 1,
                easing: 'ease'
              }
            });
        }
      });

      seq2 = 0;
  };
  ngOnInit() {
   this.role = localStorage.getItem('role'); 
   this.getClients(); 
   this.getPapers(); 
    if (this.role=="admin")
    {
      this.getCredits(); 
    }else 

    {
      this.getEvents(); 
    }
      /* ----------==========     Daily Sales Chart initialization For Documentation    ==========---------- */
      
  }
  AddPaper()
  {
    let user_id =localStorage.getItem('id'); 
  let token = localStorage.getItem('token'); 
  this.htpp.post('http://localhost:3000/papers/ajouter',{

  'designation':this.designation , 
  'credit':this.creditID, 
  'raison':this.raison, 
  //'end_date':this.date_fin.formatted ,
  
  
  },{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result); 
   this.model.close(); 
   this.getCredits(); 
  })

  }

  open(content)
 {
 this.model= this.modalService.open(content, { size: 'sm' });
 }
 open2(content)
 {
 this.model= this.modalService.open(content, { size: 'lg' });
 }
AddEvent()
{ let user_id =localStorage.getItem('id'); 
  let token = localStorage.getItem('token'); 
  this.htpp.post('http://localhost:3000/credit/ajouter',{

  'montant':this.title , 
  'completude':0, 
  'confirm':0, 
  //'end_date':this.date_fin.formatted ,
  'client':user_id
  
  },{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result); 
   this.model.close(); 
    this.getEvents(); 
  })


}

getPapers()
{
  let token = localStorage.getItem('token'); 
  let id = localStorage.getItem('id'); 
   this.htpp.get('http://localhost:3000/papers/'+id,{headers:{
     'x-access-token':token
   }}).subscribe(data=>{
     let result:any=data; 
     console.log("data", result.result); 
     this.papers=[];
     this.papers=result.result; 
     console.log("Papers",this.papers); 
     
   }); 
}
 getEvents()
 { let token = localStorage.getItem('token'); 
 let id = localStorage.getItem('id'); 
  this.htpp.get('http://localhost:3000/credit/'+id,{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result.result); 
    this.evnt=[];
    this.evnt=result.result; 
    console.log(this.evnt); 
    
  })
 }
getCredits()
 { let token = localStorage.getItem('token'); 
 let id = localStorage.getItem('id'); 
  this.htpp.get('http://localhost:3000/credit',{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result.result);
    this.evnt=[];  
    this.evnt=result.result; 
    console.log(this.evnt); 
    
  })
 }
Reservation(event)
{
  let token = localStorage.getItem('token'); 
  let id = localStorage.getItem('id'); 
  this.htpp.delete('http://localhost:3000/credit/delete/'+event,{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result.result); 
    if (this.role=="admin")
    {
      this.getCredits(); 
    }else 

    {
      this.getEvents(); 
    }
    
  })
  

}

getClients()
{
  let token = localStorage.getItem('token'); 
  
  this.htpp.get('http://localhost:3000/credit',{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any = data ; 
    console.log("result",result.result);
    this.clients= result.result; 
  }); 
}
UpdateCredit(id,etats)
{
  let token = localStorage.getItem('token'); 
  
  this.htpp.put('http://localhost:3000/credit/update/'+id,{
    "confirm":etats
  },{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result.result); 
    if (this.role=="admin")
    {
      this.getCredits(); 
    }else 

    {
      this.getEvents(); 
    }
    
  })
}

}
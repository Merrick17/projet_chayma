import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Swal from 'sweetalert2'
@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
evnt:any=[]; 
reservation:any=[]; 
user:any=[] ; 
nom:string; 
mdp:string; 
email:string; 
prenom:string; 
date:string; 
  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.getEvents(); 
    //this.getReservation(); 
    this.getUserDetails(); 
  }
  getEvents()
  { let token = localStorage.getItem('token'); 
  let id = localStorage.getItem('id'); 
   this.http.get('http://localhost:3000/credit/'+id,{headers:{
     'x-access-token':token
   }}).subscribe(data=>{
     let result:any=data; 
     console.log("data", result.result); 
     this.evnt=result.result; 
     console.log(this.evnt); 
     
   })
  }


getUserDetails()
{
  let id =localStorage.getItem('id'); 
  let token = localStorage.getItem('token'); 
  this.http.get('http://localhost:3000/users/'+id,{headers:{
    'x-access-token':token
  }}).subscribe(data=>{
    let result:any=data; 
    console.log("data", result); 
    this.user=result.result[0];
    console.log("user",this.user);  
     this.nom=this.user.nom; 
     this.prenom=this.user.prenom; 
     this.date=this.user.datenaiss; 
     this.email =this.user.email ; 
     this.mdp=this.user.mdp ; 
  })
}

register()
  {console.log(this.mdp); 
    let id = localStorage.getItem('id');
    this.http.post("http://localhost:3000/users/update/"+id,{
      "nom":this.nom,
      "prenom":this.prenom ,
      "email":this.email,
      "mdp":this.mdp,
      "type":"client",
      "date_naiss":this.date
    }).subscribe(data=>{
      console.log(data); 
     let result:any=data; 
     Swal.fire({
      title: 'Success',
      text: 'Profil Modifier',
      type: 'success',
      confirmButtonText: 'Cool'
    })
    },err=>{
      console.log(err); 
    }); 
    /*this.authenticate().then(data=>{
      console.log(data); 
    })*/
  }
}

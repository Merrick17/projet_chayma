import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2' ; 
@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements OnInit {
  users:any=[]; 
  model:any ; 
  nom:string; 
  prenom:string ; 
  email:string ; 
  tye:number ; 
  mdp:string ; 
 date: any;
  constructor( private http:HttpClient,private modalService: NgbModal) { }

  ngOnInit() {
    this.getUsers(); 
  }
  deleteUser(id)
  {
    let token = localStorage.getItem('token'); 
    this.http.delete("http://localhost:3000/users/"+id,{
      headers:{
        'x-access-token':token
      }
    }).subscribe(data=>{
      console.log(data); 
      this.getUsers(); 
    })
  }
  getUsers()
  { let token = localStorage.getItem('token'); 
    this.http.get("http://localhost:3000/users",{
      headers:{
        'x-access-token':token
      }
    }).subscribe(data=>{
      console.log(data); 
      let res :any = data ; 
      this.users =res.result; 
      console.log("USers",this.users); 
    })
  }
  open(content)
  {
  this.model= this.modalService.open(content, { size: 'lg' });
  }
  register()
  {console.log(this.mdp); 
    this.http.post("http://localhost:3000/users/signup",{
      "nom":this.nom,
      "prenom":this.prenom ,
      "email":this.email,
      "mdp":this.mdp,
      "type":"admin",
      "date_naiss":this.date
    }).subscribe(data=>{
      console.log(data); 
     let result:any=data; 
     if(result.auth==true)
     { localStorage.setItem('id',result.UserID); 
       localStorage.setItem('token',result.token);  
      //this.router.navigate(['admin/dashboard']); 
     }else{

      Swal.fire({
        title: 'Error!',
        text: 'Email deja existe ',
        type: 'error',
        confirmButtonText: 'Cool'
      })
     } 
    },err=>{
      console.log(err); 
    }); 
    /*this.authenticate().then(data=>{
      console.log(data); 
    })*/
  }
}
